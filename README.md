Spring Boot 2 + Kafka + Mongo + Google place API Sample App
===========================================================
1. Requirement
    - JDK 8
    - Gradle
    - Kafka
    - Mongo
    - [Google Place API](https://developers.google.com/places/web-service/intro?hl=ko) key
2. Google Place API
    - 장소(place)에 대한 text 검색 요청시 최대 20개의 POI(Place Of Interest) 정보를 반환
    - 검색한 장소의 총 결과 갯수가 20개 이상인 경우 결과에서 반환된 next_page_token을 추가하여 재호출시 다음 20개의 결과를받을 수 있음
3. Flow
    - ![flow image](./flow_image.jpeg)
