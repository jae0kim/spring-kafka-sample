package api.poi.subsystem.service.kafka.impl;

import api.poi.subsystem.service.kafka.Receiver;
import api.poi.subsystem.service.web.WebClient;
import api.poi.subsystem.vo.Parameters;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ReceiverImpl implements Receiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverImpl.class);

    private WebClient webClient;

    public ReceiverImpl(WebClient webClient) {
        this.webClient = webClient;
    }

    @KafkaListener(topics = "${app.topic.poi}")
    @Override
    public void listen(@Payload String message) {
        LOGGER.info("Received Message = {}", message);
        messageConvert(message);
    }

    private void messageConvert(String message) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            Parameters parameters = objectMapper.readValue(message, Parameters.class);

            webClient.httpGetConnection(parameters);

        } catch (IOException e) {
            LOGGER.error("Message Convert to JSON ERROR : " + e.getMessage());
        }
    }
}
