package api.poi.subsystem.service.kafka;

public interface Sender {

    void send(String message);
}
