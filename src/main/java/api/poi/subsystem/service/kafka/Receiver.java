package api.poi.subsystem.service.kafka;

public interface Receiver {

    void listen(String message);
}
