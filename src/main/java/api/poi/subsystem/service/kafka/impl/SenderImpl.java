package api.poi.subsystem.service.kafka.impl;

import api.poi.subsystem.service.kafka.Sender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SenderImpl implements Sender {

    private static final Logger LOGGER = LoggerFactory.getLogger(SenderImpl.class);

    private KafkaTemplate<String, String> kafkaTemplate;

    public SenderImpl(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Value("${app.topic.poi}")
    private String topic;

    @Override
    public void send(String message) {
        LOGGER.info("Sending Message={} to Topic={} at [{}]", message, this.topic, new Date());
        kafkaTemplate.send(topic, message);
    }
}
