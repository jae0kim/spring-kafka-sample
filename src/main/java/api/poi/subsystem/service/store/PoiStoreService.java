package api.poi.subsystem.service.store;

import api.poi.subsystem.vo.result.TotalResult;

public interface PoiStoreService {

    void storePOI(TotalResult totalResult, String query);
}
