package api.poi.subsystem.service.store.impl;

import api.poi.subsystem.entity.Result;
import api.poi.subsystem.repository.MongoRepsitory;
import api.poi.subsystem.service.kafka.Sender;
import api.poi.subsystem.service.store.PoiStoreService;
import api.poi.subsystem.vo.result.TotalResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PoiStoreServiceImpl implements PoiStoreService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PoiStoreServiceImpl.class);

    private MongoRepsitory mongoRepsitory;
    private Sender sender;

    @Value("${api.sleep}")
    private long sleepTime;

    public PoiStoreServiceImpl(MongoRepsitory mongoRepsitory, Sender sender) {
        this.mongoRepsitory = mongoRepsitory;
        this.sender = sender;
    }

    @Override
    public void storePOI(TotalResult totalResult, String query) {

        List<Result> resultList = totalResult.getResults();
        String nextPageToken = totalResult.getNextPageToken();

        if (resultList.size() == 0) {
            LOGGER.warn("Result size : 0");
            return;
        }

        mongoRepsitory.saveAll(resultList);

        if (nextPageToken != null) {
            Map<String, String> sendMessage = new HashMap<>();
            sendMessage.put("query", query);
            sendMessage.put("nextPageToken", nextPageToken);

            try {
                String message = new ObjectMapper().writeValueAsString(sendMessage);

                Thread.sleep(this.sleepTime);
                sender.send(message);
            } catch (JsonProcessingException e) {
                LOGGER.error("NPT JsonProcessing Error : {}", e.getMessage());
            } catch (InterruptedException e) {
                LOGGER.error("Sleep Interrupt : {}", e.getMessage());
            }
        }

    }
}
