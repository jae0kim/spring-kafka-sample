package api.poi.subsystem.service.web;

import api.poi.subsystem.vo.Parameters;

public interface WebClient {

    void httpGetConnection(Parameters parameters);
}
