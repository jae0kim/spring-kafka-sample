package api.poi.subsystem.service.web.impl;

import api.poi.subsystem.service.store.PoiStoreService;
import api.poi.subsystem.service.web.WebClient;
import api.poi.subsystem.vo.Parameters;
import api.poi.subsystem.vo.result.TotalResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WebClientImpl implements WebClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebClientImpl.class);

    @Value("${api.url}")
    private String apiUrl;

    @Value("${api.key}")
    private String apiKey;

    @Value("${api.sleep}")
    private int sleepTime;

    private PoiStoreService poiStoreService;

    public WebClientImpl(PoiStoreService poiStoreService) {
        this.poiStoreService = poiStoreService;
    }

    @Override
    public void httpGetConnection(Parameters parameters) {
        LOGGER.info(parameters.toString());

        StringBuilder apiFullyURL = new StringBuilder(this.apiUrl);
        apiFullyURL.append("key=");
        apiFullyURL.append(this.apiKey);
        apiFullyURL.append("&");
        apiFullyURL.append("query=");
        apiFullyURL.append(parameters.getQuery());

        if (parameters.getNextPageToken() != null) {
            apiFullyURL.append("&");
            apiFullyURL.append("nextPageToken=");
            apiFullyURL.append(parameters.getNextPageToken());
        }

        LOGGER.info(apiFullyURL.toString());

        RestTemplate restTemplate = new RestTemplate();
        TotalResult result = restTemplate.getForObject(apiFullyURL.toString(), TotalResult.class);

        if (result == null) {
            LOGGER.warn("API Result is null");
        } else {
            LOGGER.info("Status : {}", result.getStatus());
            poiStoreService.storePOI(result, parameters.getQuery());
        }

    }
}
