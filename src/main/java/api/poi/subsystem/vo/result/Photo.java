package api.poi.subsystem.vo.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Photo {
    private int height;
    private int width;
    @JsonProperty("html_attributions")
    private List<Object> htmlAttributions;
    @JsonProperty("photo_reference")
    private String photoReference;
}
