package api.poi.subsystem.vo.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AltId {
    @JsonProperty("place_id")
    private String placeId;
    private String scope;
}
