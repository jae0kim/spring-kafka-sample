package api.poi.subsystem.vo.result;

import api.poi.subsystem.entity.Result;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class TotalResult {

    @JsonProperty("html_attributions")
    private List<Object> htmlAttributions;
    private List<Result> results;
    private StatusConstant status;
    @JsonProperty("next_page_token")
    private String nextPageToken;
}
