package api.poi.subsystem.vo.result;

import lombok.Data;

@Data
public class OpeningHours {
    private boolean openNow;
}
