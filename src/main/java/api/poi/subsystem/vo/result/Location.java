package api.poi.subsystem.vo.result;

import lombok.Data;

@Data
public class Location {
    private double lat;
    private double lng;
}
