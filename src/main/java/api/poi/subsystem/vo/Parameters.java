package api.poi.subsystem.vo;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class Parameters {

    @NotNull
    private String query;

    private String nextPageToken;
}
