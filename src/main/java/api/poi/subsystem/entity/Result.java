package api.poi.subsystem.entity;

import api.poi.subsystem.vo.result.AltId;
import api.poi.subsystem.vo.result.Geometry;
import api.poi.subsystem.vo.result.OpeningHours;
import api.poi.subsystem.vo.result.Photo;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@ToString
@Document(collection = "result")
public class Result {

    @Id
    private String Id;

    @JsonProperty("place_id")
    private String placeId;
    private Geometry geometry;
    private String icon;
    private String id;
    private String name;
    @JsonProperty("opening_hours")
    private OpeningHours openingHours;
    private List<Photo> photos;
    private String scope;
    @JsonProperty("alt_ids")
    private List<AltId> altIds;
    private String reference;
    private List<String> types;
    private String vicinity;
}
