package api.poi.subsystem.repository;

import api.poi.subsystem.entity.Result;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoRepsitory extends CrudRepository<Result, String> {
}
