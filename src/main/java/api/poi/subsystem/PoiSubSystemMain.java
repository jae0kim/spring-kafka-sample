package api.poi.subsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoiSubSystemMain {
    public static void main(String[] args) {
        SpringApplication.run(PoiSubSystemMain.class);
    }
}
